from django.db import models

# Create your models here.

class articulo(models.Model):
	nombre = models.CharField(max_length=50)
	tipo = models.CharField(max_length=30)
	descripcion = models.CharField(max_length=100)
	fechaCreacion = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.nombre

class cliente(models.Model):
	nombre = models.CharField(max_length=50)
	descripcion = models.CharField(max_length=100)
	domicilio = models.CharField(max_length=150)
	telefono = models.IntegerField()
	fechaCreacion = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.nombre

class proveedor(models.Model):
	nombre = models.CharField(max_length=50)
	descripcion = models.CharField(max_length=100)
	domicilio = models.CharField(max_length=150)
	telefono = models.IntegerField()
	fechaCreacion = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.nombre

class almacen(models.Model):
	nombre = models.CharField(max_length=50)
	descripcion = models.CharField(max_length=100)
	fechaCreacion = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.nombre

class compra(models.Model):
	articulo = models.ForeignKey('articulo', on_delete=models.CASCADE)
	proveedor = models.ForeignKey('proveedor', on_delete=models.CASCADE)
	almacen = models.ForeignKey('almacen', on_delete=models.CASCADE)
	fechaCreacion = models.DateTimeField(auto_now_add=True, auto_now=False)
	cantidad = models.IntegerField()
	descripcion = models.CharField(max_length=100)

	def __str__(self):
		return self.descripcion

class inventario(models.Model):
	articulo = models.ForeignKey('articulo', on_delete=models.CASCADE)
	almacen = models.ForeignKey('almacen', on_delete=models.CASCADE)
	compra = models.ForeignKey('compra', on_delete=models.CASCADE)
	cantidad = models.IntegerField()

	def __str__(self):
		return str(self.almacen)

class venta(models.Model):
	articulo = models.ForeignKey('articulo', on_delete=models.CASCADE)
	inventario = models.ForeignKey('inventario', on_delete=models.CASCADE)
	cantidad = models.IntegerField()
	cliente = models.ForeignKey('cliente', on_delete=models.CASCADE)
	fechaCreacion = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return str(self.id)
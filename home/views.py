from django.shortcuts import render
from django.core.serializers import serialize
from django.views import generic
from .models import articulo, cliente, proveedor, compra, almacen, inventario, venta
from .formularios import registroArticulo, registroCliente, registroProveedor, registroCompra, registroAlmacen, registroInventarioEntrada, registroVenta
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.http import JsonResponse
from django.core.serializers.json import DjangoJSONEncoder

# Create your views here.

class LazyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, json):
            return str(obj)
        return super().default(obj)

def index(request):
	context = {}
	return render(request, "home/index.html", context)

#def articulo(request):
#	context = {}
#	return render(request, "home/articulos.html", context)

class articuloVista(generic.CreateView):
	model = articulo
	form_class = registroArticulo
	template_name = 'home/articulos2.html'
	success_url = reverse_lazy('home_view')

class clienteVista(generic.CreateView):
	model = cliente
	form_class = registroCliente
	template_name = 'home/cliente.html'
	success_url = reverse_lazy('home_view')

class proveedorVista(generic.CreateView):
	model = proveedor
	form_class = registroProveedor
	template_name = 'home/proveedor.html'
	success_url = reverse_lazy('home_view')

class compraVista(generic.CreateView):
	model = compra
	form_class = registroCompra
	template_name = 'home/compra.html'
	success_url = reverse_lazy('home_view')

class almacenVista(generic.CreateView):
	model = almacen
	form_class = registroAlmacen
	template_name = 'home/almacen.html'
	success_url = reverse_lazy('home_view')

def listaArticulosVista(request):
	articulos = articulo.objects.all()
	context = {"listaArticulos":articulos}
	return render(request, "home/listaArticulos.html", context)

def datosArticulo(request, articulo_id):
	articuloObjeto = articulo.objects.filter(id=articulo_id)
	datos = serialize('json', articuloObjeto)
	return HttpResponse(datos)

def cargarAlmacenes(request):
	almacenes = inventario.objects.select_related('almacen').select_related('compra').filter(articulo=request.POST["idArticulo"])
	context = { "form":almacenes }
	return render(request, "home/opcionesAlmacene.html", context)

def compraVistaAlternativa(request):
	formulario = registroCompra(request.POST or None)
	context = {"form" : formulario}
	retorno = "home/compra.html"
	if request.method == "POST":
		if formulario.is_valid():
			retorno = "home/index.html"
			compraGuardar = formulario.save(commit=False)
			compraGuardar.save()
			formularioInv = inventario(articulo=compraGuardar.articulo,almacen=compraGuardar.almacen,compra=compraGuardar,cantidad=compraGuardar.cantidad)
			formularioInv.save()
	return render(request, retorno, context)

def ventaVista(request):
	formulario = registroVenta(request.POST	or None)
	context = {"form":formulario}
	retorno = "home/venta.html"
	if request.method == "POST":
		if formulario.is_valid():
			retorno = "home/index.html"
	return render(request, retorno, context)

class inventarioVista(generic.ListView):
	template_name = "home/inventario.html"
	queryset = inventario.objects.select_related('compra')

def cantidadInventario(request):
	invDatos = inventario.objects.get(id=request.POST["inventarioId"])
	datos = invDatos.cantidad
	return HttpResponse(datos)

def guardarVenta(request):
	ventaGuardar = venta(cantidad=request.POST["cantidad"])
	ventaGuardar.articulo = articulo.objects.get(id=request.POST["articuloId"])
	ventaGuardar.inventario= inventario.objects.get(id=request.POST["inventarioId"])
	ventaGuardar.cliente= cliente.objects.get(id=request.POST["clienteId"])

	datosInventario = inventario.objects.get(id=request.POST["inventarioId"])
	datosInventario.cantidad = datosInventario.cantidad - int(request.POST["cantidad"])

	ventaGuardar.save()
	datosInventario.save()
	return HttpResponse("/")

def etiquetasVista(request):
	ventasObjetos = venta.objects.select_related('cliente').select_related('articulo').select_related('inventario')
	context = {"obj":ventasObjetos}
	return render(request, "home/etiquetas.html", context)

def detallesEtiquetasVista(request, ventaId):
	ventasObjetos = venta.objects.select_related('cliente').select_related('articulo').select_related('inventario').get(id=ventaId)
	almId = ventasObjetos.inventario.almacen.id
	compraId = ventasObjetos.inventario.compra.id
	almacenObjetos = almacen.objects.get(id=almId)
	compraObjetos = compra.objects.get(id=compraId)
	context = {"ventaObj":ventasObjetos, "almacenObj":almacenObjetos, "compraObj":compraObjetos}
	return render(request, "home/detallesEtiqueta.html", context)
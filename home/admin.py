from django.contrib import admin

# Register your models here.
from .models import articulo, cliente, proveedor, compra, almacen, inventario, venta

class articuloRegsitrado(admin.ModelAdmin):
	list_display = ["nombre", "descripcion"]
	class Meta:
		model = articulo
		
admin.site.register(articulo, articuloRegsitrado)

class clienteRegistrado(admin.ModelAdmin):
	list_display = ['nombre', 'descripcion']
	class Meta:
		model = cliente

admin.site.register(cliente, clienteRegistrado)

class proveedorRegistrado(admin.ModelAdmin):
	list_display = ['nombre', 'descripcion']
	class Meta:
		model = proveedor

admin.site.register(proveedor, proveedorRegistrado)

class compraRegistrada(admin.ModelAdmin):
	list_display = ['id', 'descripcion']
	class Meta:
		model = compra
admin.site.register(compra, compraRegistrada)

class almacenRegistrado(admin.ModelAdmin):
	list_display = ['id', 'nombre']
admin.site.register(almacen, almacenRegistrado)

class inventarioRegistrado(admin.ModelAdmin):
	list_display = ['id', 'articulo', 'almacen', 'compra', 'cantidad']

admin.site.register(inventario, inventarioRegistrado)

class ventaRegistrada(admin.ModelAdmin):
	list_display = ['id', 'articulo']

admin.site.register(venta, ventaRegistrada)
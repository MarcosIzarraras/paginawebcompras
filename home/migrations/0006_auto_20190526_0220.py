# Generated by Django 2.1.5 on 2019-05-26 02:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_auto_20190523_1850'),
    ]

    operations = [
        migrations.CreateModel(
            name='cliente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('descripcion', models.CharField(max_length=100)),
                ('domicilio', models.CharField(max_length=150)),
                ('telefono', models.IntegerField()),
                ('fechaCreacion', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='proveedor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('descripcion', models.CharField(max_length=100)),
                ('domicilio', models.CharField(max_length=150)),
                ('telefono', models.IntegerField()),
                ('fechaCreacion', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='empresa',
            name='tipo',
        ),
        migrations.RemoveField(
            model_name='compra',
            name='empresa',
        ),
        migrations.DeleteModel(
            name='empresa',
        ),
        migrations.DeleteModel(
            name='tipoEmpresa',
        ),
        migrations.AddField(
            model_name='compra',
            name='proveedor',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='home.proveedor'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='venta',
            name='cliente',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='home.cliente'),
            preserve_default=False,
        ),
    ]

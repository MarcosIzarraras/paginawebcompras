from django import forms
from .models import articulo, cliente, proveedor, compra, almacen, inventario, venta

class registroArticulo(forms.ModelForm):
	class Meta:
		model = articulo
		fields = ['nombre', 'tipo', 'descripcion']

class registroCliente(forms.ModelForm):
	class Meta:
		model = cliente
		fields = ['nombre', 'descripcion', 'domicilio', 'telefono']

class registroProveedor(forms.ModelForm):
	class Meta:
		model = proveedor
		fields = ['nombre', 'descripcion', 'domicilio', 'telefono']

class registroCompra(forms.ModelForm):
	class Meta:
		model = compra
		fields = ['articulo', 'proveedor', 'almacen', 'cantidad', 'descripcion']

class registroAlmacen(forms.ModelForm):
	class Meta:
		model = almacen
		fields = ['nombre', 'descripcion']

class registroInventarioEntrada(forms.ModelForm):
	class Meta:
		model = inventario
		fields = ['articulo','almacen','compra', 'cantidad']

class registroVenta(forms.ModelForm):
	class Meta:
		model = venta
		fields = ['articulo', 'inventario', 'cantidad', 'cliente']
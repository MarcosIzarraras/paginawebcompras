function cargarAlmacenes() 
{
	document.getElementById('tituloModal').textContent = "Almacenes"
	if (document.getElementById('id_articulo').name != "" && document.getElementById('id_articulo').name != "articulo")
	{
		var f = new FormData();
		f.append('idArticulo', document.getElementById('id_articulo').name);
		f.append('csrfmiddlewaretoken', tokenSeguridad);

		var almacenes = cargarGlobal('POST', '/cargarAlmacenes/', f);

		document.getElementById('contenidoModal').innerHTML = almacenes;
	}
	else{
		document.getElementById('contenidoModal').innerHTML = "";
	}
}

function cargarGlobal(metodo, url, form) {
	var retorno;
	var x = new XMLHttpRequest();

	x.open(metodo, url, false);
	x.addEventListener('load', function(e)
		{
			retorno = x.responseText;
		});
	x.send(form);
	return retorno;
}

function ObtenerCantidad(){
	if (document.getElementById('id_inventario').value != "") 
	{
		var f = new FormData();
		f.append('inventarioId', document.getElementById('id_inventario').value);
		f.append('csrfmiddlewaretoken', tokenSeguridad);
		document.getElementById('id_cantidadDisponible').value = cargarGlobal('POST', '/cantidadInventario/', f);
	}
	else{
		document.getElementById('id_cantidadDisponible').value = 0;
	}	
}

function colocarDatos(td){
	var fila = td.parentNode;
	document.getElementById('id_articulo').value = fila.children[1].textContent;
	document.getElementById('id_articulo').name = fila.children[0].textContent;

	document.getElementById('id_inventario').value = "";
	document.getElementById('id_inventario').name = "";
	document.getElementById('id_cantidadDisponible').value = 0;
	document.getElementById('btnCerrarModal').click();
}

function colocarDatosInv(td){
	var fila = td.parentNode;
	document.getElementById('id_inventario').value = fila.children[1].textContent;
	document.getElementById('id_inventario').name = fila.children[0].textContent;
	document.getElementById('id_cantidadDisponible').value = fila.children[4].textContent;
	document.getElementById('btnCerrarModal').click();
}

function guardarVenta(){
	if (validar()) 
	{
		var f = new FormData();
		f.append('articuloId', document.getElementById('id_articulo').name);
		f.append('inventarioId', document.getElementById('id_inventario').name);
		f.append('cantidad', document.getElementById('id_cantidad').value);
		f.append('clienteId', document.getElementById('id_cliente').value);
		f.append('csrfmiddlewaretoken', tokenSeguridad);

		var liga = cargarGlobal('POST', '/guardarVenta/', f);

		location.replace(liga)
	}
}

function validar()
{
	var retorno = true;

	if(document.getElementById('id_articulo').name == "") { retorno = false; }
	if(document.getElementById('id_inventario').name == "") { retorno = false; }
	if(document.getElementById('id_cantidad').value == "") { retorno = false; }
	if(document.getElementById('id_cliente').value == "") { retorno = false; }

	return retorno;
}

function soloNumeros(event) {
	var rangoCantidad = true;
	var cantidadD = document.getElementById('id_cantidadDisponible').value
	var cantidadTexto = document.getElementById('id_cantidad').value + event.key;

	if (parseInt(cantidadD) <= parseInt(cantidadTexto)) { rangoCantidad = false; }

	console.log(event);
	if ((event.charCode >= 48 && event.charCode <= 57) && (rangoCantidad)) { return true; }
	return false;
}
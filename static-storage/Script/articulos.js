var articuloId;

function cargarImagen(input)
{
	var lector = new FileReader();
	lector.onload = function(e){

		var imag = document.getElementById('imagen');
		imag.setAttribute('src', e.target.result);

	}
	lector.readAsDataURL(input.files[0]);
}

function listaArticulos()
{
	//Cambiamos titulo del modal
	document.getElementById('tituloModal').textContent = 'Articulos';

	var cuerpoModal = document.getElementById('contenidoModal');
	var x = new XMLHttpRequest();
	var f = new FormData();

	x.open('GET', '/listaArticulos');
	x.addEventListener('load', function(e)
		{
			var datos = x.responseText;
			cuerpoModal.innerHTML = datos;
		});
	x.send(f);
}

function obtenerDatosArticulo(articuloId)
{
	var datos;
	this.articuloId = articuloId;
	var x = new XMLHttpRequest();
	var f = new FormData();

	x.open('GET', '/datosArticulo/' + articuloId, false);

	x.addEventListener('load', function(e)
		{
			datos = JSON.parse(x.responseText);
		});
	x.send(f);
	return datos;
}

function guardarArticulo()
{
	var x = new XMLHttpRequest();
	var f = new FormData();

	x.open('POST', '/');
	x.addEventListener('load', function(e)
		{
			var info = x.responseText;
		});
	x.send(f);
}

//function limpiarInputs()
//{
//	var inputs = document.getElementsByTagName('INPUT');
//	for (var i = 0; i < inputs.length; i++) {
//		inputs[i].value = "";
//	}
//}
"""Proyecto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from home import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='home_view'),
    path('articulo/', views.articuloVista.as_view(), name='articulo_view'),
    path('cliente/', views.clienteVista.as_view(), name='cliente_view'),
    path('proveedor/', views.proveedorVista.as_view(), name='proveedor_view'),
    path('listaArticulos/', views.listaArticulosVista, name='listaArticulos_view'),
    path('datosArticulo/<int:articulo_id>', views.datosArticulo, name='datosArticulo_view'),
    path('compra/', views.compraVistaAlternativa, name="compraVista_view"),
    path('almacen/', views.almacenVista.as_view(), name="almacenVista_view"),
    path('inventario/', views.inventarioVista.as_view(), name='inventarioVista_view'),
    path('venta/', views.ventaVista, name='venta_view'),
    path('cargarAlmacenes/', views.cargarAlmacenes, name='cargarAlmacenes_funcion'),
    path('cantidadInventario/', views.cantidadInventario, name='cantidadInventario_funcion'),
    path('guardarVenta/', views.guardarVenta, name='guardarVenta_funcion'),
    path('etiquetas/', views.etiquetasVista, name='etiquetas_view'),
    path('detallesEtiquetas/<int:ventaId>', views.detallesEtiquetasVista, name='detallesEtiquetas_view'),
]
